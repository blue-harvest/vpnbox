# VPN Box

This script sets your Raspberry Pi device as a Wifi Access Point with VPN tunneling.

## Requirements
- Raspberry Pi Version 2 or 3, with SD card (4gb+) and power supply
- For sharing Internet from Ethernet, it is required to have a Wi-Fi adapter for Pi 2 (on Pi 3 it is on board)
- For sharing Internet from Wi-Fi, it is required to have two Wi-Fi adapters for Pi 2 (or one for Pi 3)
- Optional: touch screen.

## Installation
- Install Raspbian & start your Pi as described in the manufacture documentations.
- We will use terminal to install the required packages and run the scripts.
- If you use a touch screen, you can install on-screen keyboard by running
`sudo apt-get install matchbox-keyboard`
- Download setup.sh & browse to its directory via terminal. (e.g. /tmp)
- Run `chmod +x setup.sh`
- Run `sudo ./setup.sh`
- Reboot Pi.
- Download your OpenVPN configurations file (.ovpn / .conf). You can use VPNBook for free trial.
- Run `sudo openvpn --config YOUR_CONFIGURATION_FILE`
- Check that you can browse via a different IP to test your VPN. 
- Connect to your new hotspot & surf via VPN!

## Interface
You can create an interface for the touchscreen (e.g. with web app+node.js).
You can download multiple configuration files for OpenVPN with different server locations. And then the interface runs the OpenVPN with the desired config file according to the selected country/flag.
